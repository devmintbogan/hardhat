// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat");

async function main() {
  const DUS = await hre.ethers.getContractFactory("UpdatedStorage");
  const dus = await DUS.deploy();

  await dus.deployed();

  const value = await dus.retrieveNumber()

  console.log("UpdatedStorage with 1 ETH deployed to:", dus.address);
  console.log("UpdatedStorage value:", value);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
