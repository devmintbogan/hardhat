// SPDX-License-Identifier: MIT

pragma solidity >=0.8.0 <0.9.0;

contract Owned {
    constructor() { owner = payable(msg.sender); }
    address payable owner;
}

contract SetPersonalNumber is Owned {

    struct addressNumber {
        uint number;
        address sender;
    }

    uint public changesCount;

    addressNumber private lastSender;

    mapping(address => uint) private addressNumbers;

    constructor (uint initialNumber) Owned() {
        addPersonalNumber(initialNumber);
    }

    function addPersonalNumber (uint userNumber) public {
        addressNumbers[msg.sender] = userNumber;
        lastSender.sender = msg.sender;
        lastSender.number = userNumber;
        ++changesCount;
    }

    function checkPersonalNumber () public view returns (uint) {
        return checkAnyNumber(msg.sender);
    }

    function checkAnyNumber (address toCheck) public view returns (uint) {
        return addressNumbers[toCheck];
    }

    function checkLastAddressNumber () public view returns (addressNumber memory) {
        return lastSender;
    }

    function resetChangesMade () public {
        require(msg.sender == owner, 'Only the owner can do that, silly'); // Just for fun to require owner only do this
        changesCount = 0;
    }

}