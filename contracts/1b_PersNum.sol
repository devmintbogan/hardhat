// SPDX-License-Identifier: MIT

pragma solidity >=0.8.0 <0.9.0;

contract SetPersonalNumber {

    struct addressNumber {
        uint number;
        address sender;
    }

    addressNumber private lastSender;

    mapping(address => uint) private addressNumbers;

    constructor (uint initialNumber) {
        addPersonalNumber(initialNumber);
    }

    function addPersonalNumber (uint userNumber) public {
        addressNumbers[msg.sender] = userNumber;
        lastSender.sender = msg.sender;
        lastSender.number = userNumber;
    }

    function checkPersonalNumber () public view returns (uint) {
        return checkAnyNumber(msg.sender);
    }

    function checkAnyNumber (address toCheck) public view returns (uint) {
        return addressNumbers[toCheck];
    }

    function checkLastAddressNumber () public view returns (addressNumber memory) {
        return lastSender;
    }

}