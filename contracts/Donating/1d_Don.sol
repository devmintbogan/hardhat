// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract Owned {
    constructor() { owner = payable(msg.sender); }
    address payable owner;
}


// Contract will allow you to create a list of address to donate ETH to
// Any address can choose to pull their donation at any point and get 
// a fair percentage of the donation as defined by (1/Total Addresses)
contract Donate is Owned {

    uint public totalNumberDonations;
    uint public totalETHSent;
    uint public totalDonatableETH;
    uint public highestDonatedAmount;
    address public highestDonator;

    mapping(address => bool) public activeDonationAddresses;
    uint256 public numberOfDonationAddresses;
    mapping(address => uint256) public finalDonationAmount;

    constructor(address[] memory _donationAddresses) Owned() {
        for (uint i = 0; i < _donationAddresses.length; i++) {
            addDonatable(_donationAddresses[i]);
        }
    }

    function donate() public payable {
        require(activeDonationAddresses[msg.sender] != true, 'Donatables cannot donate funds');
        require(msg.value > 0, 'Don\'t be cheap, must send money');
        require(numberOfDonationAddresses > 0, 'No donatable addresses yet, sorry');
    
        ++totalNumberDonations;
        totalETHSent += msg.value;
        totalDonatableETH += msg.value;

        if (highestDonator == msg.sender) {
            highestDonatedAmount += msg.value;
        } else if (msg.value > highestDonatedAmount) {
            highestDonatedAmount = msg.value;
            highestDonator = msg.sender;
        }
    }

    function withdrawFunds() public payable {
        address withdrawer = msg.sender;

        require(activeDonationAddresses[withdrawer] == true, 'You are not donatable');

        uint shareOfDonatableETH = totalDonatableETH / numberOfDonationAddresses;
        // Do not allow anyone who is not an active donation address to withdraw from this account
        // Calculate the fair share of the total donatable ETH based on the remaining number of donation addresses
        // Example if there are two donation addresses, a fair share is 50% of the remaining ETH

        //Remove the address from the active donation addresses
        //Decrease the total Donatable ETH by the current share of donatable ETH
        //Decrement the number of donatable addresses
        //Update the finalDonationAmount to show how much was donnated to this address

        //Do not change this code, this is the right way to send money
        (bool success, ) = payable(msg.sender).call{value: shareOfDonatableETH}("");
        require(success, "Failed to send money");

        activeDonationAddresses[withdrawer] = false;
        --numberOfDonationAddresses;
        totalDonatableETH -= shareOfDonatableETH;
        finalDonationAmount[withdrawer] = shareOfDonatableETH;
    }

    function addDonatable(address donatable) public {
        require(msg.sender == owner, 'Owner only');
        if (activeDonationAddresses[donatable] == false && donatable != owner) {
            activeDonationAddresses[donatable] = true;
            ++numberOfDonationAddresses;
        }
    }

}