// SPDX-License-Identifier: MIT

pragma solidity >=0.8.0 <0.9.0;

contract SetPersonalNumber {

    mapping(address => uint) private addressNumbers;

    constructor (uint initialNumber) {
        addressNumbers[msg.sender] = initialNumber;
    }

    function addPersonalNumber (uint userNumber) public {
        addressNumbers[msg.sender] = userNumber;
    }

    function checkPersonalNumber () public view returns (uint) {
        return checkAnyNumber(msg.sender);
    }

    function checkAnyNumber (address toCheck) public view returns (uint) {
        return addressNumbers[toCheck];
    }

}