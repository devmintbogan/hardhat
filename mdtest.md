### Please Write the Following Important CLI Commands
##### Initializing Hardhat: `npx hardhat`
##### Creating a Hardhat Node: `npx hardhat node`
##### Create a Hardhat Console Targeting Your Local Node: `npx hardhat node --localhost`
##### Running a Script Targeting Your Local Node: `npx hardhat run`
### Please Write the Following Important HRE Javascript Commands
#### Creating a Factory Instance of a Deployed Contract: ``
#### Deploying an Instance of a Contract: `npx hardhat run scripts`
#### DeployUpdatedStorage.js --network localhost`
#### Attaching to a Running Instance of a contract located at 0x123AB: `const contract = await ethers.getContractAt('Contract', 0x123ab)`
#### Running the send with one variable 1 to a deployed contract called bank: `await bank.send(var1)`
#### Connect to address 0x9876 and running the same command above: 

``` 
const [old, new, newer] = await ethers.getSigner()
await contract.connect(newer).send(var2)
```
```
Benefits of Using Remix
1 Easy Setup
2 IDE support
...
Downsides of Using Remix
1 Can't commit to git (maybe the desktop version can?)
2 Not easy to use with complex project
...
Benefits of Using Hardhat
1 Testing 
2 ability to interact via the console with smart contracts
...
Downsides of Using Hardhat
1 Lots of setup
2 Can be complex to do tasks that are simple in Remix
...